import sys
import yaml
import subprocess

def generate_tf_job(matrix) -> str:
    tfjob = f"""
stages:
  - test

testing-farm-job:
  stage: test
  image: quay.io/testing-farm/cli:latest
  rules:
   - if: $CI_PIPELINE_SOURCE == "parent_pipeline"
  script:
    - testing-farm request
          --compose $COMPOSE
          --arch $ARCH
          --context distro=$DISTRO
          --context arch=$ARCH
          --git-ref $CI_MERGE_REQUEST_REF_PATH
          --git-url $CI_MERGE_REQUEST_PROJECT_URL
"""
    for line in map(lambda x: f'  {x}\n', yaml.safe_dump(matrix).split('\n')):
        tfjob += line
    return tfjob

def generate_empty_job() -> str:
    return """
stages:
  - test

testing-farm-job:
  stage: test
  rules:
   - if: $CI_PIPELINE_SOURCE == "parent_pipeline"
  script:
    - echo "No relevant environments to test"
"""

def main(matrix_yaml):
    with open(matrix_yaml, 'r') as configfd:
        config = yaml.safe_load(configfd.read())

    distros_matrix = {
        'parallel': {
            'matrix': []
        }
    }
    for distro in config['distros']:
        archs = distro['archs'] if 'archs' in distro else config['default_archs']
        for arch in archs:
            tmtrun = subprocess.run(
                ['tmt', '-c', f'arch={arch}', '-c', f'distro={distro["tmt-distro"]}', 'run','discover'],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.PIPE,
                text=True,
            )

            if "No tests found" in tmtrun.stderr:
                print(f"No tests relevant for distro {distro['tmt-distro']}:{arch}'")
                continue
            distros_matrix['parallel']['matrix'].append({
                "COMPOSE": distro['compose'],
                "DISTRO": distro['tmt-distro'],
                "ARCH": arch
            })

    with open('testing-farm-pipeline.yml', 'w') as outfile:
        if len(distros_matrix['parallel']['matrix']) > 0:
            outfile.write(generate_tf_job(distros_matrix))
        else:
            # If there are no relevant environments we still need to generate
            # some job to prevent the whole pipeline to fail. Related:
            # https://gitlab.com/gitlab-org/gitlab/-/issues/215100
            outfile.write(generate_empty_job())


if __name__ == "__main__":
    matrix_yaml = sys.argv[1]
    main(matrix_yaml)
