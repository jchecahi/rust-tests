# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Define package name(s) for rlAssertRpm to report installed versions
PACKAGE=$(rpm -qf $(which cargo))
PACKAGES=${PACKAGES:-$PACKAGE}
BASENAME="smoke"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm --all
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
        rlRun "mkdir .cargo"
        rlRun "export CARGO_HOME=$TmpDir/.cargo"
    rlPhaseEnd

for cratetype in binary library; do
    rlPhaseStartTest "Test $cratetype crate"
        rlRun "CRATENAME=${BASENAME}${cratetype}"

        if [ $cratetype == "library" ]; then
            rlRun "cargo new --lib $CRATENAME"
        else
            rlRun "cargo new $CRATENAME"
        fi
        rlAssertExists "./$CRATENAME"
        rlRun "pushd $CRATENAME"
        rlAssertExists "Cargo.toml"
        rlAssertExists "src"
        rlAssertGrep "$CRATENAME" Cargo.toml

        # Test building both release and debug crates
        for buildtype in debug release; do
            CMD='cargo build'
            if [ $buildtype == 'release' ]; then
              CMD+=' --release'
            fi
            rlRun "$CMD"
            if [ $cratetype == "library" ]; then
                rlAssertExists "target/$buildtype/lib$CRATENAME.rlib"
            else
                rlAssertExists "target/$buildtype/$CRATENAME"
                rlRun "target/$buildtype/$CRATENAME"
            fi
        done

        # Subcommands applicable to binary and library crates
        rlRun "cargo check"
        rlRun "cargo test"
        rlRun "cargo bench"
        rlRun "cargo doc"

        # run and install/uninstall subcommands don't apply to library crates
        if [ $cratetype == "binary" ]; then
            rlRun "cargo run"
            rlRun "cargo install --path ."
            rlRun "${CARGO_HOME}/bin/${CRATENAME}"
            rlRun "cargo uninstall $CRATENAME"
        fi

        # Ensure that cargo clean works and removes the target directory
        rlRun "cargo clean"
        rlAssertNotExists "target"

        rlRun "popd"
    rlPhaseEnd
done

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalEnd
