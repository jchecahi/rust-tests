#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="$(rpm -qf $(which rustc))"
PACKAGES=${PACKAGES:-$PACKAGE}

# This test is extracted from rust's testsuite:
# https://github.com/rust-lang/rust/tree/23405bb123681399c912552fa1c09264c0d4930d/tests/run-make/static-pie
COMPILERS=(clang gcc)

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm --all
        rlRun "tmp=\$(mktemp -d)" 0 "Create tmp directory"
        rlRun "cp test-aslr.rs $tmp"
        rlRun "pushd $tmp"
        rlRun "set -o pipefail"
    rlPhaseEnd

for compiler in "${COMPILERS[@]}"; do
    rlPhaseStartTest "test with $compiler"
        rlRun "rustc -Clinker=$compiler -Clinker-flavor=gcc --target $(uname -m)-unknown-linux-gnu -C target-feature=+crt-static test-aslr.rs"
        rlRun "readelf -l test-aslr > headers.out"
        rlAssertNotGrep INTERP headers.out
        rlAssertGrep DYNAMIC headers.out
        rlRun "./test-aslr --test-aslr"
    rlPhaseEnd
done

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $tmp" 0 "Remove tmp directory"
    rlPhaseEnd
rlJournalEnd
