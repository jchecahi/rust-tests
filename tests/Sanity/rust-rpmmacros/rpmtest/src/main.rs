use rand::Rng;

pub fn add_two(a: i32) -> i32 {
    a + 2
}

fn main() {
    let random_number = rand::thread_rng().gen_range(1..=100);
    let answer = add_two(random_number);
    println!("The answer is {}", answer);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_adds_two() {
        assert_eq!(4, add_two(2));
    }
}
