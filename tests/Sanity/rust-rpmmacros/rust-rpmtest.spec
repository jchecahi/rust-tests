%global crate rpmtest

Name:           rust-rpmtest
Version:        0.1.0
Release:        1
Summary:        Test rust rpm macros

License:        MIT
Source0:        rpmtest.tar.gz
Source1:        rpmtest-vendor.tar.gz

%if 0%{?rhel}
BuildRequires: rust-toolset
%else
BuildRequires: cargo-rpm-macros
%endif

%global _description %{expand:
%{summary}.}

%description %{_description}

%package     -n %{crate}
Summary:        %{summary}

%description -n %{crate} %{_description}

%files       -n %{crate}
%{_bindir}/rpmtest

%prep
%autosetup -n %{crate}-%{version} %{!?rhel:-a1}
# The cargo_prep macro handles vendored sources differently in rhel/fedora
%if 0%{?rhel}
%cargo_prep -V 1
%else
%cargo_prep -v vendor
%endif

%build
# cargo_build/install/test don't use -n/-a/-f in rhel/centos < 10 to avoid breaking
# existing packages using arguments.
%if 0%{?rhel} && 0%{?rhel} < 10
%cargo_build --all-features
%else
%cargo_build -a
%endif

# cargo_license(_summary) macros is not available in rust 1.73 and older.
%{?cargo_license:%cargo_license -a}
%{?cargo_license_summary:%cargo_license_summary -a}

%install
%if 0%{?rhel} && 0%{?rhel} < 10
%cargo_install --all-features
%else
%cargo_install -a
%endif

%check
%%if 0%{?rhel} && 0%{?rhel} < 10
%cargo_test --all-features
%else
%cargo_test -a
%endif
%{buildroot}/%{_bindir}/rpmtest | grep -E "The answer is [0-9]+"
